# OpenNebula

First tests to deploy VMs in Open Nebula with OpenTofu.

## What is Open Tofu ?

Open Tofu is a fork of Terraform, a IaC tool (Infrastructure as Code).

## How to use ?

You need first to init the project :

``` 
tofu init
``` 

Create a .tfvars file to populate your vars

Test your deployment steps with :

``` 
tofu plan 
``` 

When all is ready, launch your deployment with :

``` 
tofu apply 
``` 

## Links

- [OpenTofu project](https://opentofu.org/)
- [OpenNebula provider documentation](https://registry.terraform.io/providers/OpenNebula/opennebula/latest/docs)
