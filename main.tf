# Main file

# Load OpenNebula VM module

module "opennebula-vm" {
  source = "git::ssh://git@gitlab.mim-libre.fr/infrabricks/opentofu/modules/opennebula-vm.git"
  one_endpoint = var.one_endpoint
  one_username = var.one_username
  one_password = var.one_password
  one_vms_list = var.one_vms_list
}
